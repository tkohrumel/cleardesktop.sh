#! /bin/bash

DESKTOP_DIR=$HOME/Desktop

function e {
	echo $1
	echo
}

if [ $# -gt 0 ]; then
    FORMAT=$1
else 
    echo
    echo "Enter the filetype to clear from desktop"
    e "(e.g., 'png', 'txt', 'pdf', etc.):"

    read FORMAT 
    echo
fi

count=$(find $DESKTOP_DIR -maxdepth 1 -type f -name "*.$FORMAT" | wc -l | xargs)

if [[ $count > 0 ]]; then
        echo
		e "$count '$FORMAT' files(s) in $DESKTOP_DIR"

		e "Move to a subdirectory or delete? [move (m), delete (D), or quit(q)]"

    read input
    echo
else 
    e "No $FORMAT files on your desktop"
    e "Exiting to shell"

    exit 0
fi 

if [[ ("$input" == "move") || ("$input" == "Move") || ("$input" == "m") || ("$input" == "M")]]; then

    e "Please enter the name of the subdirectory to which you want to move the $count $FORMAT file(s): "
    
    read DIR_NAME
    echo

    SCREENS_DIR="$DESKTOP_DIR/$DIR_NAME"

    # create screenshot directory if it does not exist
    if [[ $DESKTOP_DIR && !(-d "$SCREENS_DIR") ]]; then
        e "Creating '$DIR_NAME' directory in $DESKTOP_DIR..."

        mkdir $SCREENS_DIR 

        e "'$DIR_NAME' directory created (path: $SCREENS_DIR)"
    fi
    
    e "Moving the $count $FORMAT file(s) to $SCREENS_DIR ..."

    mv $DESKTOP_DIR/*.$FORMAT $SCREENS_DIR

    echo "Move to '$DIR_NAME' directory complete"
    e "Exiting to shell"
elif [[ ("$input" == "delete") || ("$input" == "Delete") || ("$input" == "d") || ("$input" == "D") ]]; then

    echo "Are you sure? [y, N]"
    echo 

    read confirm

    if [[ ("$confirm" == "y") || ("$confirm" == "Y") ]]; then
        echo
        e "Deleting $count screenshot(s)..."

        rm $DESKTOP_DIR/*.$FORMAT

        e "All $FORMAT files deleted"
    else
        e "No files will be deleted." 
    fi

    e "Exiting to shell"
else 
    e "Exiting to shell"
fi

exit 0